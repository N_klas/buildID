module core.Parser;

import std.json;
import std.file;
import std.stdio;
import std.string;
import std.array;
import std.conv;
import core.models.Project;
import core.models.SourceDirectory;
import core.models.Command;
import core.VariableManager;

public class Parser {
    private string _buildFile = void;
    private Project _project = void;
    private string[string] _variables = void;
    private SourceDirectory[] _sourceDirectorys = void;
    private bool[string] _dependencies = void;
    private Command[] _prepareCommands = void;
    private Command[] _buildCommands = void;
    private Command[] _checkCommands = void;
    private Command[] _installCommands = void;

    this(string buildFile) {
        assert(exists(buildFile) == true, format("Build file does not exist! << %s", buildFile));
        this._buildFile = buildFile;
    }

    int parse() {
        import std.path : dirName;

        chdir(_buildFile.dirName);

        string buildFileContent = readText(_buildFile);
        auto parsedBuildFile = parseJSON(buildFileContent);

        _project = Project(parsedBuildFile["project"]["name"].str, parsedBuildFile["project"]["author"].str,
                parsedBuildFile["project"]["license"].str,
                parsedBuildFile["project"]["version"].str);

        foreach (var; parsedBuildFile["variables"].array) {
            _variables[var["name"].str] = var["value"].str;
        }

        foreach (dir; parsedBuildFile["sourceDirs"].array) {
            _sourceDirectorys ~= new SourceDirectory(dir["path"].str,
                    dir["recursive"].type == JSON_TYPE.TRUE);
        }

        foreach (dep; parsedBuildFile["dependencies"].array) {
            _dependencies[dep["lib"].str] = dep["buildRequirement"].type == JSON_TYPE.TRUE;
        }

        VariableManager.init(_variables, _sourceDirectorys);

        auto functionContent = parsedBuildFile["functions"];

        foreach (prep; functionContent["prepare"].array) {
            string[] reqs = [];
            foreach (req; prep["requirements"].array) {
                reqs ~= req.str;
            }
            _prepareCommands ~= new Command(prep["command"].str, reqs);
        }

        foreach (build; functionContent["build"].array) {
            string[] reqs = [];
            foreach (req; build["requirements"].array) {
                reqs ~= req.str;
            }
            _buildCommands ~= new Command(build["command"].str, reqs);
        }

        foreach (check; functionContent["check"].array) {
            string[] reqs = [];
            foreach (req; check["requirements"].array) {
                reqs ~= req.str;
            }
            _checkCommands ~= new Command(check["command"].str, reqs);
        }

        foreach (install; functionContent["install"].array) {
            string[] reqs = [];
            foreach (req; install["requirements"].array) {
                reqs ~= req.str;
            }
            _installCommands ~= new Command(install["command"].str, reqs);
        }

        debug {
            writeln("Project: ", _project);
            writeln("Variables: ", _variables);
            writeln("SourceDirectorys: ", _sourceDirectorys.length);
            writeln("Dependencies: ", _dependencies);

            writeln("Parsing functions...");
            writeln("PrepareCommands: ", _prepareCommands.length);
            writeln("BuildCommands: ", _buildCommands.length);
            writeln("CheckCommands: ", _checkCommands.length);
            writeln("Executin functions...");
        }
        else {
            writeln("Building ", _project.name);
        }

        bool success = true;
        if (success) {
            foreach (command; _prepareCommands) {
                if (success)
                    success = command.execute();
                else {
                    writeln("ERROR while preparing: ", command.command);
                    break;
                }
            }
        }
        if (success) {
            foreach (command; _buildCommands) {
                if (success)
                    success = command.execute();
                else {
                    writeln("ERROR while building: ", command.command);
                    break;
                }
            }
        }
        if (success) {
            foreach (command; _checkCommands) {
                if (success)
                    success = command.execute();
                else {
                    writeln("ERROR while checking: ", command.command);
                    break;
                }
            }
        }
        if (success) {
            writeln("Successfully build!");
        }
        else {
            writeln("Build failed!");
            return -1;
        }
        return 1;
    }

    bool install() {
        writeln("Installing ", _project.name);
        bool success = true;
        foreach (command; _installCommands) {
            if (success)
                success = command.execute();
            else {
                writeln("ERROR while installing: ", command.command);
                break;
            }
        }
        writeln("Successfully installed ", _project.name);
        return success;
    }

}
