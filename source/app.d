import std.getopt;
import std.stdio;
import core.Parser;

int main(string[] args) {
	string buildFile = "";

	auto helpInformation = getopt(args, "b|buildfile", "Specify buildfile", &buildFile);

	if (helpInformation.helpWanted) {
		defaultGetoptPrinter("buildID - Copyright © 2018, Niklas Stambor", helpInformation.options);
		writeln("Default buildfile: <project-root>build.bid");
	}
	Parser parser = new Parser(buildFile != null ? buildFile : "build.bid");
	parser.parse();

	if (args.length == 2 && args[1] == "install") {
		version (linux) {
			import core.sys.posix.unistd;

			if (getuid() != 0) {
				writeln("Please run buildID as root to install packages!");
				return -1;
			}
		}
		else {
			writeln("Current OS is not supported for installing packages!");
			return -1;
		}
		parser.install();
	}
	return 0;
}
